{
module Lexer (lexer) where
}

%wrapper "basic"

$digit = 0-9			-- digits
$alpha = [a-zA-Z]		-- alphabetic characters

tokens :-

  $white+				;
  "--".*				;
  \(					{ const TokLeftPar }
  \)					{ const TokRightPar }
  "fun"					{ const TokFun }
  "->"					{ const TokArrow }
  \-? $digit $digit* (\. $digit+)+	{ TokFloat . read }
  \+	      				{ const TokAdd }
  \-					{ const TokSub }
  \*					{ const TokMul }
  \/					{ const TokDiv }
  \,					{ const TokComma }
  "let"					{ const TokLet }
  "in"					{ const TokIn }
  $alpha [$alpha \_ \']*		{ TokId }

{

data Token =
  TokLeftPar
  | TokRightPar
  | TokFun
  | TokArrow
  | TokFloat Float
  | TokAdd
  | TokSub
  | TokMul
  | TokDiv
  | TokComma
  | TokLet
  | TokIn
  | TokId String
  deriving (Eq, Show)

lexer = alexScanTokens

}