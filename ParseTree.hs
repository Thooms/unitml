
-- Expression with dimensions.
data Expr =
  Var String
  | Const Float
  | Op String
  | Fun String Expr
  | App Expr Expr
  | Pair Expr Expr
  | Let String Expr Expr
  deriving (Eq, Show)