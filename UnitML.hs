import qualified Data.Map as M

(!) = (M.!)

{-
Dimensions, dimensioned AST and conversion function.
-}



{- 
Untyped AST and interpreter.
-}

type Env = M.Map String Value

-- Untyped expression.
data Expr =
  Var String
  | Const Float
  | Op String
  | Fun String Expr
  | App Expr Expr
  | Pair Expr Expr
  | Let String Expr Expr
  deriving (Eq, Show)
           
-- Output value for the interpreter.
data Value =
  VConst Float
  | VOp String
  | VPair Value Value
  | VFun String Env Expr
  deriving (Eq, Show)
    
-- Interpret an operator
getOp :: String -> (Float -> Float -> Float)
getOp op = case op of
  "+" -> (+)
  "-" -> (-)
  "*" -> (*)
  "/" -> (/)
    
-- Main interpreting function (gets the value of an entire expression).
value :: Env -> Expr -> Value
value env expr = case expr of
  Const x -> VConst x
  Op op -> VOp op
  Pair e1 e2 -> VPair (value env e1) (value env e2)
  Var v -> env ! v
  Let x e1 e2 -> value (M.insert x (value env e1) env) e2
  Fun x e -> VFun x env e
  App e1 e2 -> case value env e1 of
    VFun x clos e -> value (M.insert x (value env e2) clos) e
    VOp "fst" -> case value env e2 of
      VPair v1 _ -> v1
      _ -> error "Not a pair."
    VOp "snd" -> case value env e2 of
      VPair _ v2 -> v2
      _ -> error "Not a pair."
    VOp op -> case value env e2 of
      VPair (VConst v1) (VConst v2) -> VConst $ (getOp op) v1 v2 
      _ -> error "Not a pair of constants."
      
{-
let x = 1.0 + 20. in (\y -> y + y) x
-}
      
test = value M.empty $ 
       Let "x" 
       (App (Op "+") (Pair (Const 1.0) (Const 20.0)))
       (App (Fun "y" (App (Op "+") (Pair (Var "y") (Var "y"))))
        (Var "x"))
       
test2 = value M.empty $
        Let "f"
        (Fun "x" (Var "x"))
        (Var "f")
      